<?php

namespace App\Http;

class ApiResponse
{

	protected static $status = 200;

	public static function setStatus(int $statusCode)
	{
		$response = new static;
		
		$response::$status = $statusCode;

		return $response;
	}

	public static function successResponse($data)
	{

		$response =  [
			'data' => $data,
			'status' => self::$status
		];

		return response()->json($response,self::$status);
	}

	public static function errorResponse($message)
	{

		$response =  [
			'message' => $message,
			'status' => self::$status
		];

		return response()->json($response,self::$status);
	}


}