<?php

namespace App\Http\Controllers;
use App\Repositories\ClassificationNumber\Interfaces\ClassificationNumber;
use App\Http\ApiResponse;

class NumbersController extends Controller
{

    public function getClassification($number, ClassificationNumber $classification)
    {
        try {

            return ApiResponse::successResponse([
                'number' => $number,
                'classification' => $classification->getClassification($number)
            ]);
            
        } catch (\Exception $e) {
            
            return ApiResponse::setStatus(500)->errorResponse($e->getMessage());
        }
    }

    public function classification()
    {
        return view('classification');
    }
    
}
