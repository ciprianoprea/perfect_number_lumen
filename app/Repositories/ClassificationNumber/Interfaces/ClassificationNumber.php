<?php  

namespace App\Repositories\ClassificationNumber\Interfaces;

interface ClassificationNumber
{
	public function getClassification(int $number);
}