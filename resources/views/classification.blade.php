<!DOCTYPE html>
<html>
<head>
	<title>Check classification number</title>
	<link rel="stylesheet" type="text/css" href="css/app.css">
</head>
<body>

	<div class = "container" id = 'classification' v-cloak >
		
		<h1 class = "text-center">Check your classification number</h1>
		<form method="POST" v-on:submit.prevent="check" class = "classification-form col-md-6 offset-md-3">
			<input type="number" name = 'number' v-model = "number" class="classification-form--input form-control"></input>
			<button type="submit" class="btn btn-success" >CHECK</button>
		</form>

		<h2 v-if = "status && !errorMessage" class="text-center text-uppercase">
			Your number is: <span v-text = "status" class="text-success"></span>
		</h2>

		<h2 v-if = "errorMessage" class="text-center text-uppercase text-danger" v-text = "errorMessage"></h2>

	</div>

	<script type="text/javascript" src = "js/app.js"></script>

</body>
</html>