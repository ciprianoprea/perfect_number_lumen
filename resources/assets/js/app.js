window._ = require('lodash');
window.Popper = require('popper.js').default;

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');

} catch (e) {}

window.Vue = require('vue');
window.axios = require('axios');



new Vue({
	el: '#classification',
	data : {
		number	 :'',
		status   :'',
		errorMessage   :''
	},

	methods:
	{
		check:function(obj){

			this.errorMessage = false;
			var data = new FormData();
			data.set('number', this.number);

			axios.post(`api/classification/${this.number}`,
				data,
				{ headers: {'Content-Type': 'multipart/form-data' }}
    		
    		).then((response => (this.status = response.data.data.classification)))

			.catch((error => {
			    this.errorMessage = error.response.data.message
			}));
		}
	}
})